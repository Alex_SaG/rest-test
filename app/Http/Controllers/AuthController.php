<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\AuthRequest;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    /**
     * Auth user and return token
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function login(Request $request) {
        $http = new \GuzzleHttp\Client;

        try {
            $response = $http->post(config('services.passport.login_endpoint'), [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => config('services.passport.client_id'),
                    'client_secret' => config('services.passport.client_secret'),
                    'username' => $request->username,
                    'password' => $request->password
                ]
            ]);

            return [
                'token' => json_decode((string) $response->getBody(), true)['access_token'],
            ];
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            if($e->getCode() === 400) {
                return response()->json('Invalid request. Enter username or password', $e->getCode());
            } elseif ($e->getCode() === 401) {
                return response()->json('Wrong username or password', $e->getCode());
            }

            return response()->json('Something went wrong', $e->getCode());
        }
    }

    /**
     * Store a newly created user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function register(AuthRequest $request) {
        $request->validated();

        $new_user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $request->username = $request->email;
        $token = self::login($request)['token'];

        return [
            'token' => $token,
            'user' => $new_user
        ];
    }
}
