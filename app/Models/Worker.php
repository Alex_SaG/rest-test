<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Worker extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['login', 'name', 'email', 'image', 'about', 'type', 'position_id'];

    /**
     * Get the position that belong to user position.
     */
    public function position(){
        return $this->belongsTo('App\Models\Position');
    }
}
