<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Worker::class, function (Faker $faker) {
    return [
        'login' => $faker->userName,
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'image' => $faker->imageUrl(500, 500),
        'about' => $faker->realText(100),
        'type' => $faker->word(1),
        'position_id' => $faker->numberBetween($min = 1, $max = 8),
    ];
});
