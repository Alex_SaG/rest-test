<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert
        $data = array(
            array('name' => 'Отдел разработки'),
            array('name' => 'Отдел продаж'),
            array('name' => 'Отдел управления проектами'),
            array('name' => 'Отдел продвижения'),
        );

        // Model::insert($data); // Eloquent approach
        DB::table('departments')->insert($data); // Query Builder approach
    }
}
