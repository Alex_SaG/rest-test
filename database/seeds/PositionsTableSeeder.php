<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array('name' => 'Разработчик', 'department_id' => 1),
            array('name' => 'Начальник отдела', 'department_id' => 1),
            array('name' => 'Дизайнер', 'department_id' => 1),
            array('name' => 'Тестировщик', 'department_id' => 1),
            array('name' => 'Менеджер по продажам', 'department_id' => 2),
            array('name' => 'Аккаунт-менеджер', 'department_id' => 2),
            array('name' => 'Проект-менеджер', 'department_id' => 3),
            array('name' => 'Менеджер по продвижению', 'department_id' => 4),
        );

        // Model::insert($data); // Eloquent approach
        DB::table('positions')->insert($data);
    }
}
