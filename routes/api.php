<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/auth/login', 'AuthController@login');
Route::post('/auth/register', 'AuthController@register');

Route::middleware('auth:api')->get('/workers', 'WorkerController@index');
Route::middleware('auth:api')->get('/worker/{id}', 'WorkerController@show');

Route::middleware('auth:api')->get('/departments', 'DepartmentController@index');

Route::middleware('auth:api')->get('/user', 'UserController@index');
Route::middleware('auth:api')->put('/user', 'UserController@store');