#!/bin/bash
status="$(git status | grep -i 'git add <file>...')"
isCommitted="$(git status | grep -i 'Changes to be committed')"
hasCommits="$(git status | grep -i 'to publish your local commits')"
hasConflict=false

# Проверяем, есть ли файлы для коммита или сами коммиты, которых нету в master
if [ -z "$status" ] && [ -z "$isCommitted" ] && [ -z "$hasCommits" ]
then
    echo 'Изменений нет. Можно запускать pipeline.'
else
    echo 'Изменения есть. Сохраняем...'

    # Удаляем ветку hotfix/client-change, если ее забыли удалить
    git branch -D hotfix/client-change > /dev/null 2>&1
    git push origin -d hotfix/client-change > /dev/null 2>&1

    # Создаем ветку hotfix/client-change со всеми правками
    git checkout -b hotfix/client-change > /dev/null 2>&1
    git add -A > /dev/null 2>&1
    git commit -m 'Изменения клиента с прода' > /dev/null 2>&1
    git push origin hotfix/client-change > /dev/null 2>&1
    echo 'Ветка hotfix/client-change создана.'

    # Переходим на ветку develop и пробуем загрузить туда ветку hotfix/client-change
    git fetch origin > /dev/null 2>&1
    git checkout develop > /dev/null 2>&1
    git reset --hard origin/develop > /dev/null 2>&1
    git pull origin develop > /dev/null 2>&1
    devMerge="$(git merge -m 'hotfix/client-change into develop' hotfix/client-change | grep -w 'Merge conflict')"

    # Если конфликтов нету, то выгружаем ветку develop, если есть, то откатываем merge
    if [ -z "$devMerge" ]
    then
        git push origin develop > /dev/null 2>&1
        echo 'Ветка develop обновлена.'
    else
        hasConflict=true
        git reset --hard origin/develop > /dev/null 2>&1
        echo 'Конфликт в ветке develop. Загрузите ветку hotfix/client-change в develop вручную.'
    fi

    # Переходим на ветку master и пробуем загрузить туда ветку hotfix/client-change
    git checkout master > /dev/null 2>&1
    git reset --hard origin/master > /dev/null 2>&1
    git pull origin master > /dev/null 2>&1
    masterMerge="$(git merge -m 'hotfix/client-change into master' hotfix/client-change | grep -i 'Merge conflict')"

    # Если конфликтов нету, то выгружаем ветку master, если есть, то откатываем merge
    if [ -z "$masterMerge" ]
    then
        git push origin master > /dev/null 2>&1
        echo 'Ветка master обновлена.'
    else
        hasConflict=true
        git reset --hard origin/master > /dev/null 2>&1
        echo 'Конфликт в ветке master. Загрузите ветку hotfix/client-change в master вручную.'
    fi

    # Если конфликтов нету, то удаляем ветку hotfix/client-change, если есть, то пишем список действий
    if [ "$hasConflict" = true ]
    then
        echo 'Решите конфликты вручную и удалите ветку hotfix/client-change. После этого можно запускать pipeline.'
    else
        git branch -D hotfix/client-change > /dev/null 2>&1
        git push origin -d hotfix/client-change > /dev/null 2>&1
        echo 'Ветка hotfix/client-change удалена. Можно запускать pipeline.'
    fi
fi
